// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
      apiKey: "AIzaSyDWYVaEci42R0_hq9Fe_tK-UybCMYkmAIY",
      authDomain: "brains-692ae.firebaseapp.com",
      databaseURL: "https://brains-692ae.firebaseio.com",
      projectId: "brains-692ae",
      storageBucket: "brains-692ae.appspot.com",
      messagingSenderId: "763860946356",
      appId: "1:763860946356:web:cb93622da2b4556c9836e1",
      measurementId: "G-LPLX1LH2QG"
    }
  }
;

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
