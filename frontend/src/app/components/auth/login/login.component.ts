import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error = false;
  errorMesage = '';

  constructor(private as: AuthService, private router:Router) {
  }

  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  ngOnInit() {
    this.as.signOut();

  }

  onLogin(form) {

    this.as.login(form).then(res => {

      this.as.setValuesLogin();
      this.error = false;
      setTimeout(() => this.router.navigate(['/ramdomusers']), 500);

    }).catch(err => {

      this.error = true;
      this.errorMesage = err;
    });



  }


}
