import { Component, OnInit } from '@angular/core';
import {RamdomuserService} from "./ramdomuser.service";
import {RamdomUser} from '../../../shared/models/ramdomUser.interface';
import {AuthService} from "../../../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ramdomuser',
  templateUrl: './ramdomuser.component.html',
  styleUrls: ['./ramdomuser.component.scss']
})
export class RamdomuserComponent implements OnInit {

  users : RamdomUser[];
  user : RamdomUser;
  modal = false;

  constructor( private rus: RamdomuserService, private router :Router) {
    //fake guard
    if ( !localStorage.getItem('tokenId')){
      this.router.navigate(['/login'])
    }
  }

  ngOnInit() {

    this.getUsers();

  }

  getUsers() {
        this.rus.getRamdomusers().subscribe(
      res => this.users = res,
      err => console.log(err),
      ()=> console.log(this.users)
    )
  }
}
