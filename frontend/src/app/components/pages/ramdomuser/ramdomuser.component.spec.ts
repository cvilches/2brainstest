import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RamdomuserComponent } from './ramdomuser.component';

describe('RamdomuserComponent', () => {
  let component: RamdomuserComponent;
  let fixture: ComponentFixture<RamdomuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RamdomuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RamdomuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
