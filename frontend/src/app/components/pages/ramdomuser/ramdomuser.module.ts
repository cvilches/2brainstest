import { NgModule } from '@angular/core';

import { RamdomuserRoutingModule } from './ramdomuser-routing.module';
import { RamdomuserComponent } from './ramdomuser.component';
import {SharedModule} from '../../../shared/shared.module';
import {RamdomuserService} from "./ramdomuser.service";


@NgModule({
  declarations: [RamdomuserComponent],
  imports: [
    SharedModule,
    RamdomuserRoutingModule
  ],
  providers:[RamdomuserService]
})
export class RamdomuserModule { }
