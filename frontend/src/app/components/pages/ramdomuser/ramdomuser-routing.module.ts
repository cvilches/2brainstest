import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RamdomuserComponent } from './ramdomuser.component';

const routes: Routes = [{ path: '', component: RamdomuserComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RamdomuserRoutingModule { }
