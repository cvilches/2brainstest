import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {RamdomUser} from '../../../shared/models/ramdomUser.interface';

@Injectable({
  providedIn: 'root'
})
export class RamdomuserService {

  constructor(private http: HttpClient) {
  }


  getRamdomusers(): Observable<RamdomUser[]> {
    const tokenId = localStorage.getItem('tokenId');
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization", `FirebaseToken ${tokenId}`);
    const url = 'http://127.0.0.1:5000/api/user/';
    return this.http.get<RamdomUser[]>(url, {headers}).pipe()
  }
}
