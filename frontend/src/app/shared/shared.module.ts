import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClarityModule} from "@clr/angular";
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {LoginRoutingModule} from "../components/auth/login/login-routing.module";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule

  ], exports: [
    CommonModule,
    FormsModule,
    ClarityModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }

}
