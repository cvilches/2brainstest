import {Injectable} from '@angular/core';
import {User} from '../models/user.interface';

import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';
import * as firebase from "firebase";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userdata: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth) {
    this.userdata = this.afAuth.authState;
  }

  login(user: User) {
    const {email, password} = user;

    return this.afAuth.auth.signInWithEmailAndPassword(email, password)

  }

  setValuesLogin() {
    this.afAuth.auth.currentUser.getIdToken(true)
      .then((token) => localStorage.setItem('tokenId', token));

    localStorage.setItem('refreshToke', this.afAuth.auth.currentUser.refreshToken);
    localStorage.setItem('email', this.afAuth.auth.currentUser.email);
  }

  signOut() {
    this.afAuth
      .auth
      .signOut();

    localStorage.removeItem('tokenId');
    localStorage.removeItem('refreshToke');
    localStorage.removeItem('email');

    return true;
  }


}
