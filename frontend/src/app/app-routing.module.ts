import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from "./components/layout/layout.component";
import {SharedModule} from "./shared/shared.module";




const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'ramdomusers', pathMatch: 'full' },
      { path: 'ramdomusers', loadChildren: () => import('./components/pages/ramdomuser/ramdomuser.module').then(m => m.RamdomuserModule) },

    ]
  },

 { path: 'login', loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule) },
  {path: '**', redirectTo: ''}
];



@NgModule({
  imports: [
    SharedModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
