import pyrebase, logging, requests, json, random, redis
from flask import Flask, request, render_template, jsonify
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

LOG_FILENAME = './debug.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)

config = {
    "apiKey": "AIzaSyDWYVaEci42R0_hq9Fe_tK-UybCMYkmAIY",
    "authDomain": "brains-692ae.firebaseapp.com",
    "databaseURL": "https://brains-692ae.firebaseio.com",
    "projectId": "brains-692ae",
    "storageBucket": "brains-692ae.appspot.com",
    "messagingSenderId": "763860946356",
    "appId": "1:763860946356:web:cb93622da2b4556c9836e1",
    "measurementId": "G-LPLX1LH2QG"
}

pb = pyrebase.initialize_app(config)
auth = pb.auth()

rds = redis.Redis(host='localhost', port=6379, password='password123', db=0)


def get_data():
    data = rds.get('users')

    while data is None:
        if random.randrange(0, 9) != 1:

            url = 'https://randomuser.me/api/?results=12&nat=es'
            data = requests.get(url).content
            rds.set('users', data, ex=60)
            logging.debug('extract 10 spanish users from ramdomuser.me')

        else:
            logging.error('web communication error randomuser.me')
            data = None
    print(data)
    return json.loads(data)['results']


def FirebaseAuthentication(rqst):
    try:
        authorization = rqst.headers['Authorization'].split(' ')

        if authorization[0] == 'FirebaseToken':
            id_token = authorization[-1]
            auth.get_account_info(id_token)
            logging.info('valid tocken')
        else:
            logging.debug('invalid token format')
            return False

        return True

    except:
        logging.debug('invalid token')

    return False



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/signin/', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        try:
            return dict(auth.sign_in_with_email_and_password(email, password))

        except:
            print("Variable x is not defined")

            return jsonify({"error": {"context": 'null', "message": "Invalid credentials supplied.", "code": 401}})
    return render_template('signin.html')


@app.route('/api/user/')
def get_users():
    if not FirebaseAuthentication(request):
        return jsonify({"error": {"message": "Not Authorized", "code": 401}})
    users = get_data()

    return jsonify(users)


@app.errorhandler(404)
def page_not_found(error):
    '''error 404'''
    app.logger.error('Pagina no encontrada')
    return jsonify({"error": {"context": 'null', "message": "Not found", "code": 404}})


if __name__ == '__main__':
    app.run(debug=True)
